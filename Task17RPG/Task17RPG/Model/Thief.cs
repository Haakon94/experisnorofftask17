﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17RPG.Model {

    class Thief : RPGCharacter {

        #region constructor
        public Thief() {

        }

        public Thief(string name, string gender) : base(name, gender) {
            this.Energy = 200;
            this.Hp = 90;
            this.ArmorRating = 100;
        }
        #endregion

        #region behaviours
        // Methods attack and move overriding from parentclass
        public override void Attack() {
            Console.WriteLine("Attack is done by thief");
        }

        public override void Move() {
            Console.WriteLine("Move is done by thief");
        }
        #endregion
    }
}
