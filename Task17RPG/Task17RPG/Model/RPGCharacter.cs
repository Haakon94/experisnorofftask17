﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17RPG.Model {

    abstract class RPGCharacter {
        // Parent class for character

        #region variables
        private int hp;
        private int energy;
        private int armorRating;
        private string gender;
        private string name;

        #endregion
        // Constructors takes in name and gender
        #region constructor
        public RPGCharacter() {

        }

        public RPGCharacter(string name, string gender) {
            this.name = name;
            this.gender = gender;
        }

        #endregion

        #region behaviours
        // Methods attack and move
        public virtual void Attack() {

        }

        public virtual void Move() {

        }
        #endregion

        #region get and set
        public string Name { get => name; set => name = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Energy { get => energy; set => energy = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }
        public string Gender { get => gender; set => gender = value; }
        #endregion
    }
}
