﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17RPG.Model {

    class Wizard : RPGCharacter {

        #region constructor
        public Wizard () {

        }
        public Wizard(string name, string gender) : base(name, gender) {
            this.Energy = 700;
            this.Hp = 50;
            this.ArmorRating = 300;
        }

        #endregion

        #region behaviours
        // Methods attack and move overriding from parentclass
        public override void Attack() {
            Console.WriteLine("Attack is done by wizard");
        }

        public override void Move() {
            Console.WriteLine("Move is done by wizard");
        }
        #endregion


    }
}
