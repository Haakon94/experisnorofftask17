﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17RPG.Model {

    class Warrior : RPGCharacter {

        
        #region constructor
        public Warrior() {

        }

        public Warrior(string name, string gender) :base(name, gender) {
            this.Energy = 500;
            this.Hp = 100;
            this.ArmorRating = 200;
        }
        #endregion

        // Methods attack and move overriding from parentclass
        #region behaviours
        public override void Attack() {
            Console.WriteLine("Attack is done by warrior");
        }

        public override void Move() {
            Console.WriteLine("Move is done by warrior");
        }
        #endregion
    }
}
